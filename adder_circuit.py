def NAND_gate(a, b):
  if a:
    if b:
      return 0
  return 1



def NOT_gate(a):
  if NAND_gate(a, a):
    return 1
  else:
    return 0



def AND_gate(a, b):
  return NOT_gate(NAND_gate(a, b))


def OR_gate(a, b):
  return NAND_gate(NAND_gate(a, a), NAND_gate(b,b))
  #nand1 -- 1,1 ka 0 nand 2 -- 1,1 ka 0
  #main nand 0,0 ka 1

def XOR_gate(a,b):
  return AND_gate(NAND_gate(a,b),OR_gate(a,b))



def half_adder(a,b):
    s = XOR_gate(a,b)
    c = AND_gate(a,b)
    return s,c
 
 
print("#half adder")
print("A:0,B:0| Output{0} ".format(half_adder(0,0)))
print("A:0,B:1| Output{0} ".format(half_adder(0,1)))
print("A:1,B:0| Output{0} ".format(half_adder(1,0)))
print("A:1,B:1| Output{0} ".format(half_adder(1,1)))

def full_adder(a,b,t):
  s = half_adder(a,b)
  s_out = s[0]
  c = half_adder(s_out,t)

  c_out = OR_gate(s[1],c[1])
  return(c_out,c[0])
  

print("\n")
print("#full adder")

print("A:0,B:0,C:0| Output{0} ".format(full_adder(0,0,0)))
print("A:0,B:0,C:1| Output{0} ".format(full_adder(0,0,1)))
print("A:0,B:1,C:0| Output{0} ".format(full_adder(0,1,0)))
print("A:0,B:1,C:1| Output{0} ".format(full_adder(0,1,1)))
print("A:1,B:0,C:0| Output{0} ".format(full_adder(1,0,0)))
print("A:1,B:0,C:1| Output{0} ".format(full_adder(1,0,1)))
print("A:1,B:1,C:0| Output{0} ".format(full_adder(1,1,0)))
print("A:1,B:1,C:1| Output{0} ".format(full_adder(1,1,1)))




def ALU(a,b,c,opcode):
  if opcode:
    return full_adder(a,b,c)
  else:
    return half_adder(a,b)

print("\n")

print("#ALU Choice based")
print("A:1,B:1,C:1,Opcode:1| Output{0}".format(ALU(1,1,1,1)))
print("A:1,B:1,Opcode:0| Output{0}".format(ALU(1,1,0,0)))
print("A:1,B:1,C:1,Opcode:1| Output{0}".format(ALU(1,1,1,1)))
print("A:1,B:1,Opcode:0| Output{0}".format(ALU(1,1,1,0)))







  

